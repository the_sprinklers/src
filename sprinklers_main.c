/*
  The_Sprinklers 
*/
#define _TEST
#ifdef _TEST
  #include "simpletools.h"
#else
  #include <propeller.h>
#endif
#include "dht22.h"
#include "servo.h"
#include "sirc.h"
#include "string.h"

void blink();
void blink2();

serial *lcd;
const int ON = 22;
const int CLR = 12;
int main()
{
  cog_run(blink, 128);
  cog_run(blink2, 128);
  sirc_setTimeout(1000);  
  
  while(1)
  { 
    lcd = serial_open(4, 4, 0, 19200);
    writeChar(lcd, ON);
    writeChar(lcd, CLR);
    
    int t,h,c;
    c = dht22_read(3);
    t = dht22_getTemp(FAHRENHEIT);
    h = dht22_getHumidity();   
    
    dprint(lcd, "Temperature=%.1fHumidity=%.1f\n", t/10.0, h/10.0);                                     
   // print("%c remote button = %d%c\n", HOME, sirc_button(10), CLREOL);
    int button = sirc_button(10);
    pause(1000);  
    if(button == 3)
    {
      print("Temperature = %.1fF, humidity = %.1f\n", t/10.0, h / 10.0);
    } 
    if(button == 1)
    {
     print("Waterpump is on!\n");
    }
    if(button == 2)
    {
     print("Waterpump is off!\n");
    }                                         
  }  

}       

 void blink()
 {
  
  while(1)
  { 
  char x;  
  int button = sirc_button(10);
  if(button == 1)
    {
    servo_angle(15, 18300);
    high(14);
    }    
    if(button == 2)
    {
    servo_angle(0, 18300);
    low(14);
    }    
  }
}

 void blink2()
 {
  while(1)
  {
  int button = sirc_button(10);
  if(button == 1)
   {
    servo_angle(0, 18300);
    high(1);
   }   
  if(button == 2)
   {
    servo_angle(0, 18300);
    low(1);
   }    
  }
} 