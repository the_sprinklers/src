#!/usr/bin/env python
import serial
import time

ser = serial.Serial(
        port='/dev/ttyUSB0',
        baudrate=115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1,
        xonxoff=True
)
time.sleep(5)
print("Welcome The_Sprinklers Plant Irrigation System\n")
print("Turn on motor press 1\n")
print("Turn off motor press 2\n")
print("Check Temperature & Humidity press 3\n")

while 1:
        while ser.inWaiting():
                x=ser.readline()
                print (x)